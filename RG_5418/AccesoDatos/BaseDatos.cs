﻿using Dapper;
using GP.Core.Database;
using NLog;
using Oracle.DataAccess.Client;
using RG_5418.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace RG_5418.AccesoDatos
{
    class BaseDatos : IDisposable
    {
        private DbSchema dbSchema;
        private Logger logger;
        const string PaqueteArchivos = "Globalprod.PKG_RG5418";

        private const string queryGetConsumos = @"
        SELECT
        MARCACODI,
        CONSUCUPON,
        SOLINUME,
        ADINUME,
        TARJENUME,
        CONSUCUOTASPLAN,
        AUTOID,
        MOVIMCODI,
        AUTOFECHA ,
        CONSUFECHA,
        MONECODI,
        CONSUIMPOR,
        CONSUCOMPRO,
        CONSUPRESEFECHA,
        CONSUPROCEFECHA,
        CONSUMONEORIISO,
        CONSUMONEORIIMPOR,
        CONSUCOMERISOID,
        CONSUCOMERISODESCRI,
        REDTIPOCODI,
        VISA_TID,
        PAGAOTORENTICODI,
        MCC,
        AUTOIMPORTOTAL,
        PERSODOCUNUMEOTRO
        FROM GLOBALPROD.CONSUMOSRG5418 WHERE PROCESADO = 0
        ORDER BY CONSUPRESEFECHA ASC";

        private const string queryInsertConsumos = @"
        INSERT INTO CONSUMOSRG5418(MARCACODI, CONSUCUPON, SOLINUME, ADINUME, TARJENUME, MOVIMCODI, CONSUCUOTASPLAN, AUTOID, AUTOFECHA
                                        , MONECODI, CONSUIMPOR, CONSUCOMPRO, CONSUPROCEFECHA, CONSUMONEORIISO, CONSUMONEORIIMPOR
                                        , CONSUCOMERISOID, CONSUCOMERISODESCRI, REDTIPOCODI, PAGAOTORENTICODI, MCC, AUTOIMPORTOTAL, PERSODOCUNUMEOTRO)
        SELECT AUTORIZACION.MARCACODI, AUTORIZACION.AUTOID, AUTORIZACION.SOLINUME, AUTORIZACION.ADINUME, AUTORIZACION.TARJENUME, AUTORIZACION.MOVIMCODI, AUTORIZACION.PLANCODI, AUTORIZACION.AUTOID, AUTORIZACION.AUTOFECHA
                        , AUTORIZACION.MONECODI, AUTORIZACION.AUTOIMPOR, AUTORIZACION.AUTOCOMPRO, AUTORIZACION.AUTOPROCEFECHA, AUTORIZACION.AUTOMONEORIISO, AUTORIZACION.AUTOMONEORIIMPOR
                        , AUTORIZACION.AUTOCOMERISOID, AUTORIZACION.AUTOCOMERISODESCRI, AUTORIZACION.REDTIPOCODI, AUTORIZACION.PAGAOTORENTICODI, AUTORIZACION.AUTOMCC, AUTORIZACION.AUTOIMPORTOTAL, PERSONA.PERSODOCUNUMEOTRO
                FROM GLOBALPROD.AUTORIZACION
                INNER JOIN GLOBALPROD.SOCIOACTU ON SOCIOACTU.SOLINUME = AUTORIZACION.SOLINUME
        INNER JOIN GLOBALPROD.PERSONA ON PERSONA.PERSONUME = SOCIOACTU.PERSONUME
        INNER JOIN GLOBALPROD.padronrg5418 ON TO_CHAR(padronrg5418.CUITBENEFICIARIO) = PERSONA.PERSODOCUNUMEOTRO
        WHERE AUTORIZACION.AUTOFECHA > to_date(:pFECHADESDE || ' 00:00:00', 'dd/mm/yyyy hh24:mi:ss') 
        AND   AUTORIZACION.AUTOFECHA < to_date(:pFECHAHASTA ||' 23:59:59', 'dd/mm/yyyy hh24:mi:ss')
        AND AUTORIZACION.PAGAOTORENTICODI IN (:pENTIDADES)
        AND AUTORIZACION.TIPOPRODUCCODI IN (87,85)
        AND AUTORIZACION.MOVIMCODI IN (501, 801)
        AND AUTORIZACION.MONECODI = 1
        AND AUTORIZACION.MARCACODI = :pMARCA
        AND AUTORIZACION.ESTADOCODI = 0
        AND AUTORIZACION.AUTOMCC IN (select MCCPADRON.COD_MCC from globalprod.MCCrg5418 MCCPADRON)";

        private const string queryInsertAcumula = @"
        INSERT INTO ACUMULARG5418 (MARCACODI,SOLINUME,PRESEFECHA,IMPORPERCI,IMPORTOTALPERCI,PAGAOTORENTICODI,COMERISOID,COMERISODESCRI, PERSODOCUNUMEOTRO) 
        values (:pMARCACODI, :pSOLINUME, :pPRESEFECHA, :pIMPORPERCI, :pIMPORTOTALPERCI, :pPAGAOTORENTICODI, :pCOMERISOID, :pCOMERISODESCRI, :pPERSODOCUNUMEOTRO)";

        private const string queryInsertAjuste = @"
        INSERT INTO AJUSTESRG5418 (PAGAOTORENTICODI, SUCURENTICODI, COBRAENTICODI, SUCURCOBRAENTICODI, MONECODI, COMPROBANTE, 
        MOVIMFECHA, SOLINUME, IMPORAJUS, CONCEPTAJUS, DEBCRED, CANTCUOTAS, CONSUFECHA, IMPORCONSU, MCC, AUTOIMPORTOTAL, PERSODOCUNUMEOTRO) values (:pPAGAOTORENTICODI, :pSUCURENTICODI, :pCOBRAENTICODI, 
        :pSUCURCOBRAENTICODI, :pMONECODI, :pCOMPROBANTE, :pMOVIMFECHA, :pSOLINUME, :pIMPORAJUS, :pCONCEPTAJUS, :pDEBCRED, :pCANTCUOTAS, :pCONSUFECHA, :pIMPORCONSU, :pMCC, :pAUTOIMPORTOTAL, :PERSODOCUNUMEOTRO)";

        private const string queryGetSumAcun = @"
        SELECT MAX(importotalperci) AS ACUM FROM GLOBALPROD.ACUMULARG5418 
        WHERE MARCACODI = :pMARCACODI 
        AND PERSODOCUNUMEOTRO = :pPERSODOCUNUMEOTRO
        AND MONTH_(PRESEFECHA) = :pMES 
        AND YEAR(PRESEFECHA) = :pANIO";

        private const string queryGetNumAjuste = @"
        SELECT NUM_COMPROBANTE FROM GLOBALPROD.GP_COMPROBANTE_RG5418";

        private const string queryInsertNumAjuste = @" 
        UPDATE GLOBALPROD.GP_COMPROBANTE_RG5418 SET NUM_COMPROBANTE = :pNUM_COMPROBANTE";

        private const string queryGetAjuste = @"
        SELECT
        PAGAOTORENTICODI,
        SUCURENTICODI,
        COBRAENTICODI,
        SUCURCOBRAENTICODI,
        MONECODI,
        COMPROBANTE,
        MOVIMFECHA,
        SOLINUME,
        IMPORAJUS,
        CONCEPTAJUS,
        DEBCRED,
        CANTCUOTAS,
        CONSUFECHA,
        IMPORCONSU,
        MCC,
        AUTOIMPORTOTAL,
        PERSODOCUNUMEOTRO
        FROM GLOBALPROD.AJUSTESRG5418
        WHERE PAGAOTORENTICODI = :pPAGAOTORENTICODI
        AND MOVIMFECHA >= to_date(:pFECHADESDE || ' 00:00:00', 'dd/mm/yyyy hh24:mi:ss') 
        AND MOVIMFECHA < to_date(:pFECHAHASTA || ' 23:59:59', 'dd/mm/yyyy hh24:mi:ss')";

        private const string queryUpdateConsumosRG5418 = @"
        UPDATE GLOBALPROD.CONSUMOSRG5418 SET PROCESADO = 1 WHERE PROCESADO = 0";

        private const string queryGetEnvio = @"
        SELECT SEC_GPC_R3020D_DET.NEXTVAL FROM DUAL
        ";

        private static string GetCadenaConexion()
        {
            var descifrado = File.ReadAllBytes(ConfigurationManager.AppSettings["conexionDB"]);
            var encoding = new UTF8Encoding();
            var crypto = new Crypt3Des.CTripleDESUtil();
            return crypto.DesEncriptar(descifrado);
            //return "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.87.2.67)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=GPINFUAT))); User Id=Globalprod;Password=AndaGp.20;";
        }

        public BaseDatos(string cadenaConexion, Logger logger)
        {
            this.logger = logger;
            this.dbSchema = new DbSchema(cadenaConexion);
        }

        public void Dispose()
        {
            this.dbSchema.Dispose();
        }

        public decimal GetSumaAcum(int iMarcacodi, string cuit, string mes, string anio, ref decimal dSumaAcum)
        {
            var cadenaConexion = GetCadenaConexion();
            using (OracleConnection conn = new OracleConnection(cadenaConexion))
            {
                conn.Open();
                var param = new DynamicParameters();
                param.Add(":pMARCACODI", iMarcacodi);
                param.Add(":pPERSODOCUNUMEOTRO", cuit);
                param.Add(":pMES", mes);
                param.Add(":pANIO", anio);
                dynamic result = conn.Query(queryGetSumAcun, param).SingleOrDefault();
                if(result.ACUM == null)
                {
                    dSumaAcum = 0;
                }
                else
                {
                    dSumaAcum = result.ACUM;
                }
                
                conn.Dispose();
                conn.Close();
            }

            return dSumaAcum;
        }

        //Inserta en el acumulador
        public void InsertAcumula(ACUMULARG5418 acumula)
        {
            try
            {
                var cadenaConexion = GetCadenaConexion();
                using (OracleConnection conn = new OracleConnection(cadenaConexion))
                {
                    conn.Open();

                    var param = new DynamicParameters();
                    param.Add(":pMARCACODI", acumula.MARCACODI);
                    param.Add(":pSOLINUME", acumula.SOLINUME);
                    param.Add(":pPERSODOCUNUMEOTRO", acumula.PERSODOCUNUMEOTRO);
                    param.Add(":pPRESEFECHA", acumula.PRESEFECHA);
                    param.Add(":pIMPORPERCI", acumula.IMPORPERCI);
                    param.Add(":pIMPORTOTALPERCI", acumula.IMPORTOTALPERCI);
                    param.Add(":pPAGAOTORENTICODI", acumula.PAGAOTORENTICODI);
                    param.Add(":pCOMERISOID", acumula.COMERISOID);
                    param.Add(":pCOMERISODESCRI", acumula.COMERISODESCRI);

                    int rowsAffected = conn.Execute(queryInsertAcumula, param);
                    conn.Dispose();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message);
                }

                throw new Exception("ERROR en BD - queryInsertAcumula - Cortar ejecución.", ex);
            }
        }

        //Inserta el ajuste
        public void InsertAjusta(AJUSTESRG5418 ajuste)
        {
            try
            {
                var cadenaConexion = GetCadenaConexion();
                using (OracleConnection conn = new OracleConnection(cadenaConexion))
                {
                    conn.Open();

                    var param = new DynamicParameters();
                    param.Add(":pPAGAOTORENTICODI", ajuste.PAGAOTORENTICODI);
                    param.Add(":pSUCURENTICODI", ajuste.SUCURENTICODI);
                    param.Add(":pCOBRAENTICODI", ajuste.COBRAENTICODI);
                    param.Add(":pSUCURCOBRAENTICODI", ajuste.SUCURCOBRAENTICODI);
                    param.Add(":pMONECODI", ajuste.MONECODI);
                    param.Add(":pCOMPROBANTE", ajuste.COMPROBANTE);
                    param.Add(":pMOVIMFECHA", ajuste.MOVIMFECHA);
                    param.Add(":pSOLINUME", ajuste.SOLINUME);
                    param.Add(":pIMPORAJUS", ajuste.IMPORAJUS);
                    param.Add(":pCONCEPTAJUS", ajuste.CONCEPTAJUS);
                    param.Add(":pDEBCRED", ajuste.DEBCRED);
                    param.Add(":pCANTCUOTAS", ajuste.CANTCUOTAS);
                    param.Add(":pCONSUFECHA", ajuste.CONSUFECHA);
                    param.Add(":pIMPORCONSU", ajuste.IMPORCONSU);
                    param.Add(":pMCC", ajuste.MCC);
                    param.Add(":pAUTOIMPORTOTAL", ajuste.AUTOIMPORTOTAL);
                    param.Add(":PERSODOCUNUMEOTRO", ajuste.PERSODOCUNUMEOTRO);

                    int rowsAffected = conn.Execute(queryInsertAjuste, param);
                    conn.Dispose();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message);
                }

                throw new Exception("ERROR en BD - queryInsertAjuste - Cortar ejecución.", ex);
            }
        }

        public long GetNumAjus()
        {
            try
            {
                long resultado;
                var cadenaConexion = GetCadenaConexion();
                using (OracleConnection conn = new OracleConnection(cadenaConexion))
                {
                    conn.Open();
                    dynamic res = conn.Query(queryGetNumAjuste).SingleOrDefault();
                    resultado = Convert.ToInt64(res.NUM_COMPROBANTE);
                    conn.Dispose();
                    conn.Close();
                }

                using (OracleConnection conn = new OracleConnection(cadenaConexion))
                {
                    conn.Open();
                    var param = new DynamicParameters();
                    resultado = resultado + 1;
                    param.Add(":pNUM_COMPROBANTE", resultado);
                    var result = conn.Query(queryInsertNumAjuste, param);
                    conn.Dispose();
                    conn.Close();
                }

                return resultado;
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message);
                }

                throw new Exception("ERROR en BD - GetNumAjus - Cortar ejecución.", ex);
            }
        }


        //Lista los ajustes informados
        public List<R3020D> GetAjustes(int marca, int entidad, string fecha_desde, string fecha_hasta)
        {
            try
            {
                var cadenaConexion = GetCadenaConexion();

                List<R3020D> lst = new List<R3020D>();
                var filas = new List<AJUSTESRG5418>();
                using (OracleConnection conn = new OracleConnection(cadenaConexion))
                {
                    conn.Open();
                    var param = new DynamicParameters();
                    param.Add(":pPAGAOTORENTICODI", entidad);
                    param.Add(":pFECHADESDE", fecha_desde);
                    param.Add(":pFECHAHASTA", fecha_hasta);
                    var command = new CommandDefinition(queryGetAjuste, param);
                    filas = conn.Query<AJUSTESRG5418>(command)?.ToList();
                    conn.Dispose();
                    conn.Close();
                }
                
                foreach (var fila in filas)
                {
                    int debitcredit = 1;
                    if (fila.DEBCRED == 0)
                    {
                        debitcredit = 2;
                    }

                    string filler = "";
                    var filaAjus = new R3020D()
                    {
                        tiporeg = 2,
                        tipoope = debitcredit,
                        entidad_emisora = fila.PAGAOTORENTICODI,
                        sucursal_emisora = fila.SUCURENTICODI,
                        entidad_cobradora = fila.COBRAENTICODI,
                        sucursal_cobradora = fila.SUCURCOBRAENTICODI,
                        codigo_moneda = fila.MONECODI,
                        comprobante = fila.COMPROBANTE,
                        fecha_movimiento = fila.MOVIMFECHA,
                        numero_cuenta = fila.SOLINUME,
                        importe_ajuste = fila.IMPORAJUS,
                        concepto_ajuste = fila.CONCEPTAJUS,
                        deb_cred = fila.DEBCRED,
                        cant_cuotas = fila.CANTCUOTAS,
                        fecha_consumo = fila.CONSUFECHA,
                        importe_consumo = fila.IMPORCONSU,
                        mcc = fila.MCC,
                        perso_docu_num_otro = fila.PERSODOCUNUMEOTRO,
                        filler = ""
                    };
                    lst.Add(filaAjus);
                }

                 return lst;
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message);
                }

                throw new Exception("ERROR en BD - GetAjustes - Cortar ejecución.", ex);
            }
        }

        //Proceso inicial:
        // Trunca la tabla de consumos temporales
        // Inserta los consumos entre fechas
        // Lista los consumos insertados en paso anterior
        public List<RG5418> GetDatosConsumos(string fecha_desde, string fecha_hasta, int marca, int entidad)
        {
            try
            {
                logger.Info("Cargo los consumos en la tabla consumosrg5418");
                var cadenaConexion = GetCadenaConexion();
                using (OracleConnection conn = new OracleConnection(cadenaConexion))
                {
                    conn.Open();
                    var param = new DynamicParameters();
                    param.Add(":pFECHADESDE", fecha_desde);
                    logger.Info(fecha_desde);
                    param.Add(":pFECHAHASTA", fecha_hasta);
                    logger.Info(fecha_hasta);
                    param.Add(":pENTIDADES", entidad);
                    logger.Info(entidad);
                    param.Add(":pMARCA", marca);
                    logger.Info(marca);

                    //if (entidades == "618")
                    //{
                    int rowsAffected = conn.Execute(queryInsertConsumos, param);
                    //}
                    
                    conn.Dispose();
                    conn.Close();
                }

                logger.Info("Busco los consumos a procesar");
                var filas = new List<RG5418>();
                using (OracleConnection conn = new OracleConnection(cadenaConexion))
                {
                    conn.Open();
                    var command = new CommandDefinition(queryGetConsumos);
                    filas = conn.Query<RG5418>(command)?.ToList();
                    conn.Dispose();
                    conn.Close();

                    return filas;
                }

            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message);
                }

                throw new Exception("ERROR en BD - GetDatosConsumos - Cortar ejecución.", ex);
            }
        }

        public void ActualizaConsumosRG5418()
        {
            try
            {
                var cadenaConexion = GetCadenaConexion();

                using (OracleConnection conn = new OracleConnection(cadenaConexion))
                {
                    conn.Open();
                    var ret = conn.Execute(queryUpdateConsumosRG5418);
                    logger.Info("Actualiza los consumos en tabla CONSUMOSRG5418");
                    conn.Dispose();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message);
                }

                throw new Exception("ERROR en BD - ActualizaConsumosRG5418 - Cortar ejecución.", ex);
            }
        }

        public string GetEnvio()
        {
            string envio = string.Empty;
            try
            {
                var cadenaConexion = GetCadenaConexion();

                using (OracleConnection conn = new OracleConnection(cadenaConexion))
                {
                    conn.Open();
                    long lnNextVal = Convert.ToInt64(conn.ExecuteScalar(queryGetEnvio));
                    logger.Info("Busca el valor de envio en la sequence");
                    envio = lnNextVal.ToString();
                    conn.Dispose();
                    conn.Close();
                }

                return envio;
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message);
                }

                throw new Exception("ERROR en BD - queryGetEnvio - Cortar ejecución.", ex);
            }
        }
    }
}
