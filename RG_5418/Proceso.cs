﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using System.IO;
using System.Configuration;
using System.Globalization;
using System.Threading.Tasks;
using RG_5418.AccesoDatos;
using RG_5418.Models;
using System.Diagnostics;

namespace RG_5418
{
    class Proceso
    {
        private static string fechanom = string.Empty;
        
        private static string GetCadenaConexion()
        {
            var descifrado = File.ReadAllBytes(ConfigurationManager.AppSettings["conexionDB"]);
            var encoding = new UTF8Encoding();
            var crypto = new Crypt3Des.CTripleDESUtil();
            return crypto.DesEncriptar(descifrado);
            //return "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.87.2.67)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=GPINFUAT))); User Id=Globalprod;Password=AndaGp.20;";
        }

        private static string ArmarNombreLP(string idMarca, string idEntidad)
        {
            //numero de envío??
            string fechaarc = DateTime.Now.ToString("yyyyMMdd_HHmm");
            return "R3021D_" + idMarca.PadLeft(2, '0') + "_" + idEntidad.PadLeft(3, '0') + "_" + fechaarc + ".txt";
        }
        #region RG5418
        public static void GenRG5418XEntidad(string rutaSalida, string fecha_desde, string fecha_hasta, int marca, int entidad, Logger logger)
        {
            var cadenaConexion = GetCadenaConexion();
            try
            {
                DateTime dt = DateTime.Parse(fecha_desde, new CultureInfo("es-AR"));

                string mes = dt.Month.ToString();
                string anio = dt.Year.ToString();

                using (var baseDatos = new BaseDatos(cadenaConexion, logger))
                {
                    //Lista en la tabla Comercio
                    logger.Info("Voy a buscar los consumos - Inicio");
                    List<RG5418> lst_consumos = baseDatos.GetDatosConsumos(fecha_desde, fecha_hasta, marca, entidad);
                    logger.Info("Voy a buscar los consumos - Fin");
                    ACUMULARG5418 acumula = new ACUMULARG5418();

                    string vigencia = DateTime.Now.ToString("yyyyMM");
                    Console.WriteLine("## Cantidad de registros = " + lst_consumos.Count);
                    logger.Info("## Cantidad de registros = " + lst_consumos.Count);
                    for (int i = 0; i < lst_consumos.Count; i++)
                    {
                        Stopwatch timeMeasure = new Stopwatch();
                        timeMeasure.Start();
                        Console.WriteLine("## Inicio el proceso - N° " + i);
                        logger.Info("## Inicio el proceso - N° " + i);
                        //---------------------------------------------------------------------//
                        // Calcula cuanto viene acumulando
                        //---------------------------------------------------------------------//
                        decimal montotopeacum = Convert.ToDecimal(ConfigurationManager.AppSettings["montotopeacum"]);
                        int reintegroporcen = Convert.ToInt32(ConfigurationManager.AppSettings["reintegroporcen"]);
                        decimal acumulado = 0;
                        baseDatos.GetSumaAcum(lst_consumos[i].MARCACODI, lst_consumos[i].PERSODOCUNUMEOTRO, mes, anio, ref acumulado);

                        //---------------------------------------------------------------------//
                        // Acumula en la tabla
                        //---------------------------------------------------------------------//
                        acumula.MARCACODI = lst_consumos[i].MARCACODI;
                        acumula.SOLINUME = lst_consumos[i].SOLINUME;
                        acumula.PERSODOCUNUMEOTRO = lst_consumos[i].PERSODOCUNUMEOTRO;
                        acumula.PRESEFECHA = lst_consumos[i].AUTOFECHA;
                        acumula.PAGAOTORENTICODI = lst_consumos[i].PAGAOTORENTICODI;
                        acumula.COMERISOID = lst_consumos[i].CONSUCOMERISOID;
                        acumula.COMERISODESCRI = lst_consumos[i].CONSUCOMERISODESCRI;
                        acumula.CONSUCUPON = lst_consumos[i].CONSUCUPON;
                        acumula.IMPORPERCI = lst_consumos[i].AUTOIMPORTOTAL * 21 / 100;
                        //*************************************************************************************************
                        //  Esta info es para el acumulador (unificarla con el ajuste)
                        //*************************************************************************************************
                        if (lst_consumos[i].MOVIMCODI == 801)
                        {
                            acumula.IMPORTOTALPERCI = acumulado - acumula.IMPORPERCI; //Restar movim creditos (ajus=debito)
                        }
                        else
                        {
                            acumula.IMPORTOTALPERCI = acumulado + acumula.IMPORPERCI; //Restar movim debitos (ajus=credito)
                        }
                        //*************************************************************************************************
                        //---------------------------------------------------------------------//
                        // Ajustes en la tabla
                        //---------------------------------------------------------------------//
                        AJUSTESRG5418 ajus = new AJUSTESRG5418();
                        ajus.PAGAOTORENTICODI = lst_consumos[i].PAGAOTORENTICODI;
                        ajus.SUCURENTICODI = 10; //verificar la sucursal
                        ajus.COBRAENTICODI = 10; //verificar entidad cobradora
                        ajus.SUCURCOBRAENTICODI = 10; //verificar sucursal de la entidad cobradora
                        ajus.MONECODI = lst_consumos[i].MONECODI;
                        ajus.COMPROBANTE = baseDatos.GetNumAjus(); //verificar el comprobante => guid?
                        ajus.MOVIMFECHA = lst_consumos[i].AUTOFECHA;
                        ajus.SOLINUME = lst_consumos[i].SOLINUME;
                        ajus.MCC = lst_consumos[i].MCC;
                        ajus.PERSODOCUNUMEOTRO = lst_consumos[i].PERSODOCUNUMEOTRO;
                        ajus.CANTCUOTAS = 1; //validar
                        //ajus.CONSUFECHA = lst_consumos[i].AUTOFECHA;

                        //decimal diferencia = montotopeacum - acumula.IMPORTOTALPERCI;
                        //if (diferencia == 0)
                        //{
                        //    // No hace nada (Caso 1)
                        //}
                        //else if (diferencia == lst_consumos[i].AUTOIMPORTOTAL * 21 / 100)
                        //{
                        //    // aplica el ajuste
                        //}
                        //else if (diferencia < lst_consumos[i].AUTOIMPORTOTAL * 21 / 100)
                        //{
                        //    // aplica importe de la diferencia
                        //}

                        //*************************************************************************************************
                        //  Esta info es para el ajuste (unificarla con el acumulador)
                        //*************************************************************************************************
                        if (lst_consumos[i].AUTOIMPORTOTAL < 0)
                        {
                            ajus.IMPORAJUS = (lst_consumos[i].AUTOIMPORTOTAL * -1) * 21 / 100; //calcula el 21%
                            ajus.IMPORCONSU = lst_consumos[i].AUTOIMPORTOTAL * -1;
                        }
                        else
                        {
                            ajus.IMPORAJUS = lst_consumos[i].AUTOIMPORTOTAL * 21 / 100; //calcula el 21%
                            ajus.IMPORCONSU = lst_consumos[i].AUTOIMPORTOTAL;
                        }

                        //*******************************************************************
                        //  TAMBIEN AFECTAR AL ACUMULADOR
                        //*******************************************************************
                        //Generamos el ajuste
                        if (lst_consumos[i].MOVIMCODI == 501) //Consumos
                        {
                            //505 al credito
                            //Código Ajuste: 505 | Descripción: 
                            acumula.AJUSCONCEPCODI = 505;
                            ajus.DEBCRED = 1; //1= credito
                            ajus.CONCEPTAJUS = 505;
                        }
                        if (lst_consumos[i].MOVIMCODI == 801) // Devolucion consumos
                        {
                            //506 al debito
                            //Código Ajuste: 506 | Descripción: 
                            acumula.AJUSCONCEPCODI = 506;
                            ajus.DEBCRED = 2; //2=debito
                            ajus.CONCEPTAJUS = 506;
                        }

                        //Inserta datos en acumula
                        baseDatos.InsertAcumula(acumula);
                        //Inserta datos en ajustes
                        baseDatos.InsertAjusta(ajus);

                        //logger.Info("## Fin del proceso - N° " + i);
                        timeMeasure.Stop();
                        //Console.WriteLine("## Fin del proceso - N° " + i + " Tiempo: " +timeMeasure.Elapsed.TotalMilliseconds + "ms");
                    }

                    logger.Info("llamo a la funcion que actualiza los consumos procesados (CONSUMOSRG5418)");
                    baseDatos.ActualizaConsumosRG5418();
                    //logger.Info("Complete la actualizacion de CONSUMOSRG5418");
                    logger.Info("Armo el R3021D");
                    GenerarR3021D(logger, baseDatos, marca, entidad, rutaSalida, fecha_desde, fecha_hasta);
                    logger.Info("Termine el R3021D");
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message + ": " + ex.StackTrace);
                }
            }
        }
        #endregion RG5418
        #region Reporte R3021
        private static void GenerarR3021D(Logger logger, BaseDatos baseDatos, int marca, int entidad, string rutaSalida, string fecha_desde, string fecha_hasta)
        {
            try
            {
                var nombreArchivo = ArmarNombreLP(marca.ToString(), entidad.ToString());
                var filas = baseDatos.GetAjustes(marca, entidad, fecha_desde, fecha_hasta);
                if (filas.Count > 0)
                {
                    //Header
                    string ent = entidad.ToString().PadLeft(5, '0');
                    string mar = marca.ToString().PadLeft(3, '0');
                    string filler = "";
                    string envio = baseDatos.GetEnvio();
                    string origen = "12";
                    string descripcion = "Ajustes RG5330D - RG5418";
                    string header = "1" + ent + mar + "R3021D  " + descripcion.PadRight(30, ' ') + envio.PadLeft(12, '0') + DateTime.Now.ToString("yyyyMMdd") + origen.PadLeft(5, '0') + filler.PadLeft(227, ' ');

                    using (var stream = new FileStream(rutaSalida + nombreArchivo, FileMode.Create, FileAccess.Write))
                    {
                        using (var writer = new StreamWriter(stream, Encoding.GetEncoding("ISO-8859-1"), 2 << 23))
                        {
                            decimal sumaimporpesos = 0;
                            decimal sumaimpordolar = 0;

                            logger.Info("Archivo de salida: " + nombreArchivo);
                            writer.AutoFlush = false;
                            writer.WriteLine(header);
                            foreach (var fila in filas)
                            {
                                writer.WriteLine(GenerarDetalle(fila, filler.PadLeft(204, ' ')));
                            }

                            //Trailer
                            string canreg = filas.Count().ToString();
                            string trailer = "3" + ent + "R3021D  " + canreg.PadLeft(9, '0') + sumaimporpesos.ToString().PadLeft(16, '0') + sumaimpordolar.ToString().PadLeft(16, '0') + filler.PadLeft(244, ' ');

                            writer.WriteLine(trailer);
                        }
                        logger.Info("Archivo cerrado.");
                    }
                }
               
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message);
                }
            }
        }

        private static string GenerarDetalle(R3020D fila, string filler)
        {
            var linea = new StringBuilder(500);

            linea.Append(fila.tiporeg);
            linea.Append(fila.tipoope); 
            linea.Append(fila.entidad_emisora.ToString().PadLeft(5, '0'));
            linea.Append(fila.sucursal_emisora.ToString().PadLeft(5, '0'));
            linea.Append(fila.entidad_cobradora.ToString().PadLeft(5, '0'));
            linea.Append(fila.sucursal_cobradora.ToString().PadLeft(5, '0'));
            linea.Append(fila.codigo_moneda.ToString().PadLeft(2, '0'));
            linea.Append(fila.comprobante.ToString().PadLeft(10, '0'));
            linea.Append(fila.fecha_movimiento.ToString("yyyyMMdd"));
            linea.Append(fila.numero_cuenta.ToString().PadLeft(10, '0'));
            string ret = ConvertValue(fila.importe_ajuste);
            linea.Append(ret.PadLeft(15, '0'));
            linea.Append(fila.concepto_ajuste.ToString().PadLeft(5, '0'));
            linea.Append(fila.deb_cred.ToString());
            linea.Append("001"); //ajustes sin cuotas

            //MEJORA 20/10/2023
            //linea.Append(fila.fecha_consumo.ToString("yyyyMMddHHmmss"));
            string consu = ConvertValue(fila.importe_consumo);
            linea.Append(consu.PadLeft(15, '0'));
            linea.Append(fila.mcc.ToString().PadLeft(5, '0'));
            linea.Append(fila.perso_docu_num_otro.ToString());

            linea.Append(filler);
            return linea.ToString();
        }
        #endregion Reporte R3021


        private static string ConvertValue(decimal valor)
        {
            decimal salida = 0;
            int e = 0;

            decimal data = Math.Round(valor, 2);
            if (!int.TryParse(valor.ToString(), out e))
            {
                // es decimal y no debo hacer nada
                salida = data;
            }
            else
            {
                salida = data * 100;
            }

            string retorno = salida.ToString();
            return retorno.Replace(",", "");
        }
    }
}
