﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RG_5418.Models
{
    class PADRONRG5418
    {
        public int VIGENCIA { get; set; }
        public Int64 CUITBENEFICIARIO { get; set; }
        public string CBUBENEFICIARIO { get; set; }
        public Int64 NUMEDOCU { get; set; }
    }
}
