﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RG_5418.Models
{
    class R3020D
    {
        //Layout de R3020
        public int tiporeg { get; set; }
        public int tipoope { get; set; }
        public int entidad_emisora { get; set; }
        public int sucursal_emisora { get; set; }
        public int entidad_cobradora { get; set; }
        public int sucursal_cobradora { get; set; }
        public int codigo_moneda { get; set; }
        public long comprobante { get; set; }
        public DateTime fecha_movimiento { get; set; }
        public DateTime fecha_consumo { get; set; }
        public long numero_cuenta { get; set; }
        public decimal importe_ajuste { get; set; }
        public decimal importe_consumo { get; set; }
        public int concepto_ajuste { get; set; }
        public int deb_cred { get; set; }
        public int cant_cuotas { get; set; }
        public int mcc { get; set; }
        public string perso_docu_num_otro { get; set; }
        public string filler { get; set; }
    }
}
