﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RG_5418.Models
{
    class AJUSTESRG5418
    {
		public int PAGAOTORENTICODI { get; set; }
		public int SUCURENTICODI { get; set; }
		public int COBRAENTICODI { get; set; }
		public int SUCURCOBRAENTICODI { get; set; }
		public int MONECODI { get; set; }
		public long COMPROBANTE { get; set; }
		public DateTime MOVIMFECHA { get; set; }
		public DateTime CONSUFECHA { get; set; }
		public long SOLINUME { get; set; }
		public decimal IMPORAJUS { get; set; }
		public decimal IMPORCONSU { get; set; }
		public int CONCEPTAJUS { get; set; }
		public int DEBCRED { get; set; }
		public int CANTCUOTAS { get; set; }
		public int MCC { get; set; }
		public decimal AUTOIMPORTOTAL { get; set; }
        public string PERSODOCUNUMEOTRO { get; set; }
    }
}
