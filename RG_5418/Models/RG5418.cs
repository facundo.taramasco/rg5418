﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RG_5418.Models
{
    class RG5418
    {
		public int MARCACODI { get; set; }
        public long CONSUCUPON { get; set; }
        public long SOLINUME { get; set; }
        public int ADINUME { get; set; }
        public string TARJENUME { get; set; }
		public int CONSUCUOTASPLAN { get; set; }
		public long AUTOID { get; set; }
		public int MOVIMCODI { get; set; }
        public DateTime AUTOFECHA { get; set; }
		public DateTime CONSUFECHA { get; set; }
		public int MONECODI { get; set; }
        public decimal CONSUIMPOR { get; set; }
        public string CONSUCOMPRO { get; set; }
		public DateTime CONSUPRESEFECHA { get; set; }
		public DateTime CONSUPROCEFECHA { get; set; }
		public string CONSUMONEORIISO { get; set; }
        public Decimal CONSUMONEORIIMPOR { get; set; }
		public string CONSUCOMERISOID { get; set; }
		public string CONSUCOMERISODESCRI { get; set; }
		public int REDTIPOCODI { get; set; }
		public string VISA_TID { get; set; }
		public int PAGAOTORENTICODI { get; set; }
		public int MCC { get; set; }
		public decimal AUTOIMPORTOTAL { get; set; }
		public string TRNEXTERNALID { get; set; }
		public decimal DEVOLIVA { get; set; }
        public string PERSODOCUNUMEOTRO { get; set; }
    }
}
