﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace RG_5418
{
    class Program
    {
        static void Main(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();
            string rutaSalida = string.Empty;
            string fecha_desde = string.Empty;
            string fecha_hasta = string.Empty;
            int marca = 0;
            int entidad = 0;

            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("es-AR");
            try
            {
                logger.Info("----------------------------------- COMIENZO -----------------------------------");
                logger.Info("Argumentos de entrada (args):");
                foreach (var argumento in args)
                {
                    logger.Info(argumento);
                }
                // Argumen 0 viene insertado por el Scheduller.
                if (args.Length == 6)
                {
                    logger.Info("Argumentos por Launcher");
                    logger.Info("Argumento 0: " + args[0].ToString());
                    rutaSalida = args[1];
                    fecha_desde = args[2];
                    fecha_hasta = args[3];
                    marca = int.Parse(args[4]);
                    entidad = int.Parse(args[5]);
                }
                else
                {
                    logger.Info("Argumentos por config (pisan a los argumentos de entrada del Scheduller)");
                    rutaSalida = ConfigurationManager.AppSettings["rutaSalida"];
                    logger.Info(rutaSalida);
                    fecha_desde = ConfigurationManager.AppSettings["fecha_desde"];
                    logger.Info(fecha_desde);
                    fecha_hasta = ConfigurationManager.AppSettings["fecha_hasta"];
                    logger.Info(fecha_hasta);
                    marca = int.Parse(ConfigurationManager.AppSettings["Marca"]);
                    logger.Info(marca);
                    entidad = int.Parse(ConfigurationManager.AppSettings["Entidad"]);
                    logger.Info(entidad);
                }

                logger.Info("Ruta de salida: " + rutaSalida);

                if (!rutaSalida.EndsWith(@"\"))
                {
                    rutaSalida += @"\";
                }
                //Loop por entidad

                Proceso.GenRG5418XEntidad(rutaSalida, fecha_desde, fecha_hasta, marca, entidad, logger);
                logger.Info("-------------------------------------- FIN --------------------------------------");
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message);
                }
            }
        }
    }
}
